let http = require("http");

//http is considered an object
http.createServer(function(req,res){

	res.writeHead(200,{'Content-Type':'text/plain'});
	res.end('Hello from our second server!')
}).listen(8000);

console.log('Server is running from localhost:8000');
