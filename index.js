/*Client-Server Architecture

Client-Server Architecture is a computing model wherein a 
server hosts, delivers, and manages resources that a 
client consumes.

What is a client? */

//console.log("Hello, World!");

let http = require("http");

// console.log(http);

/*
	http.createServer() method allowed us to
	create a server & handle requests of a client
	
	.createServer() method has an anonymous function that
	handles our clients & our server response. 
	-able to receive 2 objs,

*/

http.createServer(function(req,res){
	console.log(req.url);


	if(req.url === "/"){
	res.writeHead(200,{'Content-Type': 'text/plain'});
	res.end("hello!")

} else if (req.url === "/login"){
	res.writeHead(200,{'Content-Type': 'text/plain'});
	res.end("Welcome to the Login Page")
}
 else {
	res.writeHead(200,{'Content-Type': 'text/plain'});
	res.end("Resource cannot be found.")
}
}).listen(4000);

console.log("Server is running on localHost:4000")





